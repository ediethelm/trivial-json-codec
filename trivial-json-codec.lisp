;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-json-codec)

(defvar *slots-to-ignore-in-serialization* nil)

(defgeneric serialize (obj stream)
  (:documentation "Serialize an object *OBJ* into *STREAM*.
Implementations for built-in types already exist. The user might extend with methods for specific types."))

(defmethod serialize ((obj (eql nil)) stream)
  (declare (type stream stream))
  (format stream "null"))

(defmethod serialize ((obj function) stream)
  (declare (type stream stream))
  (serialize nil stream))

(defmethod serialize ((obj (eql t)) stream)
  (declare (type stream stream))
  (format stream "true"))

(defmethod serialize ((obj string) stream)
  (declare (type stream stream))
  (format stream "\"~A\"" obj))

(defmethod serialize ((obj integer) stream)
  (declare (type stream stream))
  (format stream "~A" obj))

(defmethod serialize ((obj float) stream)
  (declare (type stream stream))
  (format stream "~A" obj))

(defmethod serialize ((obj list) stream)
  (declare (type stream stream))
  (princ #\< stream)
  (iterate:iterate
    (iterate:for e in obj)
    (unless (iterate:first-iteration-p)
      (princ #\, stream))
    (serialize e stream))
  (princ #\> stream))

(defmethod serialize ((obj array) stream)
  (declare (type stream stream))
  (princ #\[ stream)
  (iterate:iterate
    (iterate:for e in-vector obj)
    (unless (iterate:first-iteration-p)
      (princ #\, stream))
    (serialize e stream))
  (princ #\] stream))

(defmethod serialize ((obj symbol) stream)
  (declare (type stream stream))
  (if (eq (symbol-package obj) (find-package 'keyword))
      (format stream "\":~:@(~a~)\"" obj)
      (format stream "~:@(~a~)" (symbol-name obj))))

(defmethod serialize ((obj standard-object) stream)
  (princ #\{ stream)
  (iterate:iterate
    (iterate:for s in (mapcar #'c2mop:slot-definition-name (c2mop:class-slots (class-of obj))))
    (when (and (not (member s *slots-to-ignore-in-serialization*)) ; Implement a way to exclude some slots from being serialized
	       (slot-boundp obj s))
      (format stream
	      "~A \"~A\" : ~A"
	      (if (iterate:first-iteration-p) "" ", ")
	      (symbol-name s)
	      (serialize-json (slot-value obj s)))))
  (princ #\} stream))

(defun serialize-json (obj)
  "Takes *OBJ* and serializes it into a string. Uses the generic *SERIALIZE* to do the job."
  (return-from serialize-json
    (with-output-to-string (stream)
      (serialize obj stream))))

(defun deserialize-raw (json-str &key (read-table nil))
  "Deserialize *JSON-STR* into a property list. As opposed to *DESERIALIZE-JSON* this function does not require a base class to deserialize."
  (unless (and json-str
	       (> (length json-str) 0))
    (return-from deserialize-raw nil))
  
  (labels ((normalize-result (parsed)
	     (unless  parsed
	       (return-from normalize-result nil))

	     (unless (consp parsed)
	       (return-from normalize-result
		 (cond ((and (stringp parsed)
			     (>= (length parsed) 2)
			     (eq #\: (char parsed 0)))
			(intern (string-upcase (subseq parsed 1)) "KEYWORD"))
		       (t parsed))))

	     (let ((head (car parsed))
		   (tail (cdr parsed)))
	       ;;(when (consp head)
		 ;;(return-from normalize-result (append (normalize-result head) (trivial-utilities:mklist (normalize-result tail)))))

	       (case head
		 (key (list (intern (string-upcase (car tail)) "KEYWORD")
			    (normalize-result (cadr tail))))
		 ((list object) (normalize-result tail))
		 (array (when tail
			  (let ((elms (normalize-result tail)))
			    (make-array (length elms) :initial-contents elms))))
		 (t (cons (normalize-result head) (normalize-result tail)))))))
    (normalize-result (consume json-str 0 read-table))))

(defun deserialize-json (json-str &key (class nil) (read-table nil) (constructors nil))
  "Reads *JSON-STR* and creates an according object.  
If *CLASS* is non-nil and represents a class, an instance of it is returned. Otherwise only built-in types can be deserialized.  
*READ-TABLE* makes it possible to inject specific readers, as pondons to *SERIALIZE*. It has the form of an alist containing the dispatch character as car and the deserialization function as cdr.  
*CONSTRUCTORS* holds an alist mapping the keyword returned by a specific reader to a object construction function."
  (declare (type simple-string json-str))

  (unless (and json-str
	       (> (length json-str) 0))
    (return-from deserialize-json nil))

  (let ((parsed (consume json-str 0 read-table)))
    (cond ((null parsed)
	   (error "Could not parse string."))
	  ((and (null class) (member 'Object (trivial-utilities:flatten parsed)))
	   (error "Cannot deserialize an object when no base class is given."))
	  (t
	   (return-from deserialize-json (create-symbol-from-json parsed class constructors))))))


(defun create-symbol-from-json (parsed base-class constructors)
  (unless parsed
    (return-from create-symbol-from-json nil))

  (when (and (not (null base-class)) (listp base-class) (not (eq (car base-class) 'or))) ;; @TODO Potential namespace conflict!
    (error "Composed slot types are only allowed with 'or'."))

  (typecase parsed
    (list
     (let ((parsed-l parsed))
       (declare (type list parsed-l))
       (return-from create-symbol-from-json
	 (case (car parsed-l)
	   (List (if (eq 1 (length parsed-l))
		     nil
		     (mapcar #'(lambda (elm) (create-symbol-from-json elm base-class constructors)) (cdr parsed-l))))
	   (Array (let ((arr (make-array (length (the list (cdr parsed-l))))))
		    (loop for elm in (cdr parsed-l)
		       for i of-type fixnum = 0 then (1+ i)
		       do (setf (elt arr i) (create-symbol-from-json elm base-class constructors)))
		    arr))
	   (Object (create-object-from-json parsed-l base-class constructors))
	   (otherwise (let ((fn (cdr (assoc (car parsed-l) constructors))))
			(if fn
			    (funcall fn (cdr parsed-l))
			    (error ""))))))))
    (simple-string
     (let ((parsed-s parsed))
       (declare (type simple-string parsed-s))
       (return-from create-symbol-from-json
	 (if (and (> (length parsed-s) 0) (eq #\: (char parsed-s 0)))
	     (intern (subseq parsed-s 1) "KEYWORD")
	     parsed-s))))
    (t
     (return-from create-symbol-from-json parsed))))


(defun create-object-from-json (parsed base-class constructors)
  (unless parsed
    (return-from create-object-from-json nil))

  (labels ((find-all-relevant-classes (base-class)
	     "Find all classes derived from base-class"
	     (let ((derived (c2mop:class-direct-subclasses base-class)))
	       (if derived
		   (remove-if #'null (append (list base-class) (mapcan #'find-all-relevant-classes derived)))
		   (trivial-utilities:mklist base-class))))

	   (extract-slots-name-and-type-from-class (class &aux (slots (c2mop:class-slots class)))
	     (values (mapcar #'c2mop:slot-definition-name slots)
		     (mapcar #'c2mop:slot-definition-type slots)))

	   (extract-slots-name-and-type-from-classes (classes &aux names types)
	     (dolist (c classes)
	       (multiple-value-bind (name type)
		   (extract-slots-name-and-type-from-class c)
		 (push (list c name) names)
		 (push (list c type) types)))
	     (values names types))

	   (match-class (parsed class-slot-names)
	     (unless (eq (car parsed) 'Object)
	       (return-from match-class nil))

	     (let*
		 ((keys (mapcar #'second (cdr parsed)))
		  (slots (mapcar #'symbol-name class-slot-names))
		  (union (union keys slots :test #'string-equal)))
	       (and (eq (length union) (length keys)) (eq (length union) (length slots)))))

	   (get-slots-name-for-match (match names)
	     (cadar (member match names :key #'car)))

	   (get-slots-type-for-match (match types)
	     (cadar (member match types :key #'car)))

	   (get-data-for-slot (slot-name parsed)
	     (third (car (member (symbol-name slot-name) (cdr parsed) :key #'second :test #'string-equal)))))

    (multiple-value-bind (slot-names slot-types)
	(extract-slots-name-and-type-from-classes (find-all-relevant-classes base-class))

      (let* ((matches (mapcar #'(lambda (x) (if (match-class parsed (second x)) (car x) nil)) slot-names))
	     (matches-count (count-if #'identity matches)))

	(unless (eq 1 matches-count)
	  (if (> matches-count 1)
	      (error "Class mapping not unique.")
	      (error "No class found to instantiate from JSON.")))

	(let* ((match (find-if #'identity matches))
	       (class match)
	       (params (mapcan #'
			(lambda (name raw-type)
			  (unless raw-type
			    (error "Null type given."))

			  (let* ((types (if (and (listp raw-type)
						 (eq 'or (car raw-type)))
					    (cdr raw-type)
					    (list raw-type)))
				 (data (get-data-for-slot name parsed))
				 (unk (gensym))
				 (res (loop for type in (remove 'null types)
					 with r = unk
					 until
					   (setf r (create-symbol-from-json data (find-class type nil) constructors))
					 finally (return r))))
			    (when (eq unk res)
			      (error "Could not match value to type."))

			    (list
			     (intern (string name) "KEYWORD")
			     res)))

			(get-slots-name-for-match match slot-names)
			(get-slots-type-for-match match slot-types))))

	  (return-from create-object-from-json (apply #'make-instance (append (list class) params))))))))


;; read-table -> ((#\# . #'read-object-reference))
(defun consume (json json-start-position read-table)
  "Consumes one element from the beginning of the string and returns the values (produced-element consumed-until)."
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum json-start-position))
  (let ((start-position (skip-blanks json json-start-position (1- (length json)))))
    (declare (type trivial-utilities:non-negative-fixnum start-position))

    (let ((start-char (char json start-position)))
      (cond
	((eq start-char #\")
	 (read-string json start-position))
	((member start-char '(#\- #\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9))
	 (read-number json start-position))
	((eq start-char #\{)
	 (parse-object json start-position read-table))
	((or
	  (eq start-char #\<)
	  (eq start-char #\[))
	 (parse-array json start-position start-char read-table))
	((or
	  (eq start-char #\t)
	  (eq start-char #\T))
	 (read-true json start-position))
	((or
	  (eq start-char #\f)
	  (eq start-char #\F))
	 (read-false json start-position))
	((or
	  (eq start-char #\n)
	  (eq start-char #\N))
	 (read-null json start-position))
	(t
	 (trivial-utilities:aif (member start-char read-table :key #'first)
				(funcall (cdar it) json start-position read-table)
				(error "Invalid data type")))))))

(defun read-string (json start-position)
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position))

  (loop for idx of-type fixnum from (1+ start-position) to (- (length json) 1)
     until (and (eq (char json idx) #\")
		(not (eq (char json (1- idx)) #\\)))
     finally (return (values (subseq json (1+ start-position) idx) (1+ idx)))))

(flet ((skip-to-next (json start-position)
	 (declare (type trivial-utilities:non-negative-fixnum start-position))
	 (loop for idx of-type fixnum from (1+ start-position) to (- (length json) 1)
	    while (alpha-char-p (char json idx))
	    finally (return idx))))

  (defun read-true (json start-position)
    (declare (type trivial-utilities:non-negative-fixnum start-position))
    (values t (skip-to-next json start-position)))

  (defun read-false (json start-position)
    (declare (type trivial-utilities:non-negative-fixnum start-position))
    (values nil (skip-to-next json start-position)))

  (defun read-null (json start-position)
    (declare (type trivial-utilities:non-negative-fixnum start-position))
    (values nil (skip-to-next json start-position))))

(defun read-number (json start-position) ;; : returns end-position
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position))
  (let ((end start-position)
	(value (make-array 0
			   :element-type 'character
			   :fill-pointer 0
			   :adjustable t)))
    (loop for idx of-type fixnum from start-position to (1- (length json))
       while (member (char json idx) '(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\. #\- #\e #\d #\f))
       do (vector-push-extend (char json idx) value)
	 (incf end))

    (multiple-value-bind (r n)
	(parse-number:parse-number value)
      (declare (ignore n))
      (return-from read-number (values r end)))))

(defun read-array (json start-position &key (open-char #\[) (close-char #\])) ;; : returns end-position
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position))

  (loop
     for idx of-type fixnum from (1+ start-position) to (- (length json) 1)
     with in-group = 1
     do
       (when (eq (char json idx) open-char)
	 (incf in-group))
       (when (eq (char json idx) close-char)
	 (decf in-group))
       (when (and (eq in-group 0) (eq (char json idx) close-char))
	 (return (1+ idx)))))


(defun parse-array (json start-position open-char read-table)
  ;; returns values ('(array|list [element]*) end-position)
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position))

  (let* ((value (ecase open-char (#\< '(List)) (#\[ '(Array))))
	 (close-char (ecase open-char (#\< #\>) (#\[ #\])))
	 (end-pos (read-array json start-position :open-char open-char :close-char close-char)))
    (declare (type fixnum end-pos))

    (unless (loop for idx of-type fixnum from start-position to (1- end-pos)
	       until (eq (char json idx) close-char)
	       unless (eq (char json idx) #\Space)
	       do (return nil)
	       finally (return t))

      (loop
	 for start of-type fixnum = (1+ start-position) then (1+ separator)
	 and separator of-type (or null fixnum) = (or
						   (position #\, json :start (1+ start-position) :end end-pos)
						   (position close-char json :start (1+ start-position) :end end-pos))
	 then (or
	       (position #\, json :start (1+ separator) :end end-pos)
	       (position close-char json :start (1+ separator) :end end-pos))
	 until (or (null separator) (>= separator end-pos))
	 do
	   (if (member (char json start) '(#\" #\< #\[))
	       ;; @TODO Read until the end of this element and move separator to the correct end position
	       (let* ((start-char (char json start))
		      (stop-char (ecase (char json start) (#\< #\>) (#\[ #\]) (#\" #\")))
		      (new-end
		       (loop
			  for idx of-type fixnum from (1+ start) to (1- (length json))
			  with in-group of-type fixnum = 1
			  do
			    (when (eq (char json idx) start-char)
			      (incf in-group))
			    (when (eq (char json idx) stop-char)
			      (decf in-group))
			    (when (and (eq (char json idx) stop-char) (or (eq in-group 0) (eq start-char stop-char)))
			      (return (1+ idx))))))

		 (if (> new-end start)
		     (multiple-value-bind (val pos)
			 (consume json start read-table)
		       (setf value (append value (list val)))
		       (setf separator pos))
		     (setf separator new-end)))
	       (unless (<= separator start)
		 (multiple-value-bind (val pos)
		     (consume json start read-table)
		   (setf separator pos)
		   (setf value (append value (list val))))))))

    (return-from parse-array (values value end-pos))))


(defun parse-object (json start-position read-table) ;; returns values ('(object [(key value)]*) end-position)
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position))

  (let ((value '(Object))
	(end-pos (read-array json start-position :open-char #\{ :close-char #\})))

    (loop while (< start-position (1- end-pos))
       ;; @TODO Here we could enter into an infinite loop!
       do
	 (multiple-value-bind (val pos) (parse-keyvalue json (1+ start-position) (1- end-pos) read-table)
	   (setf start-position pos) ;;(skip-blanks json pos (1- end-pos)))
	   (setf value (append value (list val)))))

    (values value end-pos)))


(defun parse-keyvalue (json start-position json-end-position read-table)
  (declare (type simple-string json)
	   (type trivial-utilities:non-negative-fixnum start-position json-end-position))

  (when (>= start-position (1- json-end-position))
    ;; @TODO This is actually an error!
    (return-from parse-keyvalue (values nil json-end-position)))

  (multiple-value-bind (key key-end-position)
      (consume json start-position read-table)

    (let ((colon-position (skip-blanks json key-end-position json-end-position))) ;skip-blanks is used to guarantee no other chars appear (as opposed to position #\:)
      (unless (eq (char json colon-position) #\:)
	(error "Malformed JSON string!"))

      (multiple-value-bind (value value-end-position)
	  (consume json (1+ colon-position) read-table)

	(setf value-end-position (skip-blanks json value-end-position json-end-position))

	;; Ignore the comma!
	;;(when (eq (char json value-end-position) #\,)
	;;  (incf value-end-position))

	(when (and (typep value 'simple-string)
		   (> (length value) 0)
		   (eq (char value 0) #\:))
	  (setf value (intern (subseq value 1) "KEYWORD")))

	(return-from parse-keyvalue (values `(Key ,key ,value) value-end-position))))))

(defun skip-blanks (str start end)
  (declare (type simple-string str)
	   (type trivial-utilities:non-negative-fixnum start end))
  (or
   (loop for idx from start to end
      unless (or (eq (char str idx) #\Space)
		 (eq (char str idx) #\Tab)
		 (eq (char str idx) #\NewLine))
      do (return (the fixnum idx)))
   start))
