(uiop:define-package #:trivial-json-codec
  (:documentation "trivial-json-codec provides a JSON parser able to identify class hierarchies.")
  (:use #:common-lisp)
  (:export #:serialize
	   #:serialize-json
	   #:deserialize-json
	   #:deserialize-raw
	   #:read-string
	   #:read-t
	   #:read-number
	   #:read-array
	   #:*slots-to-ignore-in-serialization*))

