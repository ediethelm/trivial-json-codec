;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-json-codec
  :name "trivial-json-codec"
  :description "A JSON parser able to identify class hierarchies."
  :long-description ""
  :version "0.3.12"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:trivial-utilities
	       :log4cl
	       :closer-mop
	       :iterate
	       :parse-number)
  :components ((:file "package")
	       (:file "trivial-json-codec")))

